Website name: localhost/balamaga/

NOTE: Below commands are important to run in order to install dependecies

git clone [link] and run 'npm install' 

CSS files are inside css folder
js file (_main.js) is inside js folder
json file (boxes.json) is inside json folder

Contact link - Is the assignment based page 
Home page - contains dynamic information from boxes.json file 

--------------- 
THEME: Enlight - Free Bootstrap Theme
AUTHOR: ProBootstrap.com
AUTHOR URI: https://probootstrap.com/
Twitter: https://twitter.com/probootstrap
Facebook: https://facebook.com/probootstrap


CREDITS:

Bootstrap
http://getbootstrap.com/

jQuery
http://jquery.com/

jQuery Easing
http://gsgd.co.uk/sandbox/jquery/easing/

HTML5Shiv
https://github.com/aFarkas/html5shiv

Google Fonts
https://www.google.com/fonts/

Icomoon
https://icomoon.io/app/

Respond JS
https://github.com/scottjehl/Respond/blob/master/LICENSE-MIT

animate.css
http://daneden.me/animate

jQuery Waypoint
https://github.com/imakewebthings/waypoints/blog/master/licenses.txt

Stellar Parallax
http://markdalgleish.com/projects/stellar.js/

Owl Carousel
https://owlcarousel2.github.io/OwlCarousel2/

FlexSlider
http://flexslider.woothemes.com

Demo Images
https://unsplash.com
https://pexels.com

Video
https://vimeo.com

PhotoSwipe
http://photoswipe.com/

Magnific Popup
http://dimsemenov.com/plugins/magnific-popup/

Isotope
https://isotope.metafizzy.co/

ImagesLoaded
https://github.com/desandro/imagesloaded