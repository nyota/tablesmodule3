-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jan 22, 2018 at 10:10 PM
-- Server version: 10.1.28-MariaDB
-- PHP Version: 7.1.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `myproject`
--

-- --------------------------------------------------------

--
-- Table structure for table `boxes`
--

CREATE TABLE `boxes` (
  `idBox` int(11) NOT NULL,
  `title` varchar(50) NOT NULL,
  `description` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `boxes`
--

INSERT INTO `boxes` (`idBox`, `title`, `description`) VALUES
(1, 'About Us', 'Balamaga Bed & Breakfast is located in a quiet area just outside and south of Bukoba town, capital of Kagera region, with one of the most beautiful gardens in North-West Tanzania overlooking Lake Victoria.'),
(2, 'Our Rooms', 'Balamaga B and Breakfast is ideal for regular business travelers, consultants as well as families and small tourist groups, who want to enjoy a quiet place to stay, conducive to work or to explore Bukoba district.'),
(3, 'Explore Bukoba', 'Bukoba town is halfway Serengeti, Mwanza and Uganda and provides an excellent stop-over between the mountain gorillas and Tanzania’s main national parks. Often called Tanzania’s most peaceful and beautiful town..');

-- --------------------------------------------------------

--
-- Table structure for table `guest`
--

CREATE TABLE `guest` (
  `idGuest` int(11) NOT NULL,
  `idRoomType` int(11) NOT NULL,
  `firstName` varchar(50) NOT NULL,
  `surname` varchar(50) NOT NULL,
  `username` varchar(10) NOT NULL,
  `password` varchar(8) NOT NULL,
  `roomName` varchar(30) NOT NULL,
  `noofRoom` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `guest`
--

INSERT INTO `guest` (`idGuest`, `idRoomType`, `firstName`, `surname`, `username`, `password`, `roomName`, `noofRoom`) VALUES
(1, 2, 'Beatice ', 'John', 'bea', 'one', 'selfdouble', 2),
(2, 1, 'Lucia L', 'Koku', 'lucy', 'two', 'selfsingle', 10),
(3, 4, 'Lyidia', 'Felix', 'lidi', 'three', 'twindouble', 5),
(4, 3, 'Devotha', 'Rwegasira', 'devo', 'four', 'twinsingle', 4),
(5, 2, 'Lucia K', 'Roberto', 'tony', 'five', 'selfdouble', 3);

-- --------------------------------------------------------

--
-- Table structure for table `roomtype`
--

CREATE TABLE `roomtype` (
  `idRoomType` int(11) NOT NULL,
  `roomname` varchar(50) NOT NULL,
  `roomPrice` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `roomtype`
--

INSERT INTO `roomtype` (`idRoomType`, `roomname`, `roomPrice`) VALUES
(1, 'selfsingle', 60),
(2, 'selfdouble', 110),
(3, 'twinsingle', 100),
(4, 'twindouble', 80);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `iduser` int(11) NOT NULL,
  `firstname` varchar(50) NOT NULL,
  `surname` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  `subject` varchar(30) NOT NULL,
  `message` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`iduser`, `firstname`, `surname`, `email`, `subject`, `message`) VALUES
(1, 'Anna', 'Nyerere', 'anna@gmail.com', '2 room', 'Available 2 rooms '),
(2, 'Beatrice', 'John', 'bea@gmail.com', 'We are all about people', 'Everyone\'s idea of the perfect housemate '),
(3, 'Diocles', 'Rwegasira', 'Dio@gmail.com', 'We are safe', 'We\'ve got a team of real people checking'),
(4, 'Lyidia', 'Koku', 'koku@gmail.com', 'We are the busiest', 'Every 3 minutes someone finds a flatmate'),
(5, 'Lucia', 'James', 'koku@yahoo.com', 'Need a room', 'Place a free Room Wanted ad');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `boxes`
--
ALTER TABLE `boxes`
  ADD PRIMARY KEY (`idBox`);

--
-- Indexes for table `guest`
--
ALTER TABLE `guest`
  ADD PRIMARY KEY (`idGuest`);

--
-- Indexes for table `roomtype`
--
ALTER TABLE `roomtype`
  ADD PRIMARY KEY (`idRoomType`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`iduser`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `boxes`
--
ALTER TABLE `boxes`
  MODIFY `idBox` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `guest`
--
ALTER TABLE `guest`
  MODIFY `idGuest` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `roomtype`
--
ALTER TABLE `roomtype`
  MODIFY `idRoomType` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `iduser` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
