$(document).ready(function() {

    
    //server name
    var server = "http://www.winelove.club";

    //addEventListener
    $('#formCreate').submit( processForm );
    $('#formEdit').submit( EditForm );
    
    $("#tablelist").on("click",".edit-item",editItem);  
    $("#tablelist").on("click",".remove-item",removeItem);  
 
    /************************* Load page **********************************/
    $.ajax({
        dataType: 'json',
        url: server + '/heroes/list.php'
        }).done(function(users){
            manageRow(users);        
        }).fail(function(errror){
            showtoast('error');
    });

    /************************* New Hero **********************************/
    function processForm(e) {
	    e.preventDefault();
        var newUser = $("#newUser").val();
        $.ajax({
            dataType: 'json',
            type:'GET',
            url: server + '/heroes/create.php',
            data:{name:newUser}
        }).done(function(users){

            $("#newUser").val('');
            $(".modal").modal('hide');
            showtoast("add",function() {
                manageRow(users); //callback
            });

        });  
    }


    /************************* Edit Hero **********************************/
    function EditForm(e) {
        e.preventDefault();
              
      var idUser = $("#idUser").val();  
      var nameUser = $("#editUser").val();

        $.ajax({
            dataType: 'json',
            type:'GET',
            url: server + '/heroes/update.php',
            data:{id:idUser,name:nameUser}
        }).done(function(users){

           alert("done")
    
                 });  
    }


    function editItem() {        
        var idUser = $(this).parent("td").data("id");
        var nameUser = $(this).closest('tr').children('td:eq(1)').text();
        
        $("#idUser").val(idUser);
        $("#editUser").val(nameUser);

        console.log(idUser);
    }


    /************************* removeItem **********************************/ 
    function removeItem() {
         if (confirm("are you sure to delete ?")) {
            var idUser = $(this).parent("td").data("id");
            
            $.ajax({
                dataType: 'json',
                type:'GET',
                url: server + '/heroes/delete.php',
                data:{id:idUser}
            }).done(function(users){

                showtoast("remove",function() {
                    manageRow(users); //callback
                });

            });  
        }

    }

    /************************* functions **********************************/
    function showtoast(action,callback){
        
        switch (action) {
            case "remove":
                $.toast({ heading: 'Remove', text: 'User has been removed', icon: 'success',hideAfter:1000})
            break; 

            case "add":
                $.toast({ heading: 'Add', text: 'User has been added', icon: 'success', hideAfter:1000})
            break; 

            case "error":
                $.toast({heading: 'Error ', text: 'Impossible to connect... ', icon: 'error',hideAfter:1000})
            break; 
        }   

        setTimeout(function(){
            callback();
        }, 1000); // after 1 second
    }

    function manageRow(users){
        $("tbody").html('');

        $.each( users, function( key, value ) {
            var	row = '';

            row = row + '<tr>';
                row = row + '<td >'+value.id+'</td>';
                row = row + '<td>'+value.name+'</td>';
                row = row + '<td data-id="' + value.id + '">';
                    row = row + '<button  data-toggle="modal" data-target="#edit-item"  class="btn btn-primary edit-item">Edit</button> ';
                    row = row + '<button  class="btn btn-danger remove-item">Delete</button></td>';
                row = row + '</td>';
            row = row + '</tr>';

            $("tbody").append(row);
        });
    }

});